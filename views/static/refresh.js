/* eslint-disable */
var config = { backendUrl: "" };

function refreshSession(cb) {
  fetch(config.backendUrl + "session/refresh", { credentials: "include", method: "POST" })
    .then(res => {
      if (res.status == 200) {
        console.log("Refreshed");
        if (cb) cb()
      } else {
        console.error(
          "Failed refreshing with status " + res.status,
          res.statusText
        );
        if (cb) cb(res.statusText)
        //window.location.href = config.backendUrl+"/session/login";
      }
    })
    .catch(err => {
      console.error("Failed refreshing", err);
      cb(err)
    });
}

function getSessionState(cb) {
  fetch(config.backendUrl + "session/state", { credentials: "include", method: "GET" })
    .then(res => {
      if (res.status != 200) {
        console.error(
          "Failed getting session state with status " + res.status,
          res.statusText
        );
        //window.location.href = config.backendUrl+"session/login";
        //throw new Error("fail getting session")
        cb(null)
      } else {
        return res.json()
      }
    }).then(state => {
      if (cb) cb(state)
    })
    .catch(err => {
      console.error("Failed refreshing", err);
      cb(null)
    });
}
let autoRefreshActivated = false;
function autoRefreshSession(failCb) {
  getSessionState(state => {
    if(state) console.log("getSessionState "+state.active)
    if (state && state.active) {
      const expiresInMs = state.expiresInMs - 5000;
      if (autoRefreshActivated) return
      setTimeout(() => {
        refreshSession((err) => {
          autoRefreshActivated = false;
          if (!err) autoRefreshSession(failCb);
          else if (failCb) failCb({ state, err })
        })
      }, expiresInMs)
      autoRefreshActivated = true;
      console.log("Next refresh in " + expiresInMs + " ms at " + new Date(Date.now() + expiresInMs))
    } else {
      if (failCb) failCb({ state })
    }
  })
}
  /* eslint-enable */
// end of js script
