

function clickMenu(menu) {
  if(!store) return 
  if(!store.menus) return
  if(typeof store.menus[menu] === "function") {
    store.menus[menu](menu)
  } else if(typeof store.menus.default === "function") {
    store.menus.default(menu)
  }
}