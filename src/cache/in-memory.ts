import { CacheInterface } from "../usecase/adapter-interfaces";

export default class CacheMemory implements CacheInterface {
  private mem: { [key: string]: any };
  private timers: { [key: string]: NodeJS.Timer };
  constructor() {
    this.mem = {};
    this.timers = {};
  }
  put(opaque: string, value: any, durationSec: number): Promise<boolean> {
    //console.log('MEMORY CACHE-PUT:', opaque, value,' as promise')
    this.mem[opaque] = value;
    this.mem["X-" + opaque] = { durationSec: durationSec, accessCount: 0 };
    if (Number.isFinite(durationSec) && durationSec > 0) {
      if (this.timers[opaque]) {
        clearTimeout(this.timers[opaque]);
      }
      this.timers[opaque] = setTimeout(() => {
        delete this.mem[opaque];
        delete this.mem["X-" + opaque];
        delete this.timers[opaque];
      }, durationSec * 1000);
    }
    return Promise.resolve(true);
  }
  get(opaque: string): Promise<any> {
    //console.log('MEMORY CACHE-GET:', opaque, this.mem[opaque],' as promise')
    if (this.mem["X-" + opaque]) {
      this.mem["X-" + opaque].accessCount++;
    }
    return Promise.resolve(this.mem[opaque]);
  }
  del(key: string): Promise<boolean> {
    delete this.mem[key];
    delete this.mem["X-" + key];
    if (this.timers[key]) {
      clearTimeout(this.timers[key]);
      delete this.timers[key];
    }
    return Promise.resolve(true);
  }
  close(): void {
    Object.values(this.timers).forEach(timer => clearTimeout(timer));
    this.timers = {};
    this.mem = {};
  }
}
