module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  roots: ["src"],
  coverageDirectory: "coverage",
  testResultsProcessor: "jest-sonar-reporter"
};
